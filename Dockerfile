FROM node:14.15.0-alpine

RUN apk add --no-cache bash

RUN apk add --no-cache jq

ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

ENV PATH=$PATH:/home/node/.npm-global/bin

RUN npm install -g @sentry/cli --unsafe-perm

RUN npm install -g appcenter-cli 

COPY . .

RUN chmod +x /pipe.sh

ENTRYPOINT ["/pipe.sh"]
