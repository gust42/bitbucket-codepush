#!/usr/bin/env bash

set -e

APPCENTER_TOKEN=${APPCENTER_TOKEN:?'APPCENTER_TOKEN environment variable missing.'}
WORKING_DIR=${WORKING_DIR:="clients/app"}
APPCENTER_IOS_IDENTIFIER=${APPCENTER_IOS_IDENTIFIER:="corviva/Workly-1"}
APPCENTER_ANDROID_IDENTIFIER=${APPCENTER_ANDROID_IDENTIFIER:="corviva/Workly"}
ENVIRONMENT=${ENVIRONMENT:="Development"}
SENTRY_AUTH_TOKEN=${SENTRY_AUTH_TOKEN:?'SENTRY_AUTH_TOKEN environment variable missing.'}
IOS_BUNDLE_ID=${IOS_BUNDLE_ID:="com.corviva.workly"}

appcenter login --token ${APPCENTER_TOKEN}
cd ${WORKING_DIR}

set +e
appcenter codepush release-react -d ${ENVIRONMENT} -a ${APPCENTER_ANDROID_IDENTIFIER} --sourcemap-output --output-dir ./build_android 2>/dev/null
appcenter codepush release-react -d ${ENVIRONMENT} -a ${APPCENTER_IOS_IDENTIFIER} --plist-file ios/workly/Info.plist --sourcemap-output --output-dir ./build_ios 2>/dev/null

ANDROID_VERSION=$(appcenter codepush deployment history Development -a ${APPCENTER_ANDROID_IDENTIFIER} --output json | jq -r '.[-1][0]')
IOS_VERSION=$(appcenter codepush deployment history Development -a ${APPCENTER_IOS_IDENTIFIER} --output json | jq -r '.[-1][0]')

# TODO: Uploaded sourcemap is 0kb
sentry-cli --auth-token=${SENTRY_AUTH_TOKEN} react-native appcenter --org corviva --project workly ${APPCENTER_ANDROID_IDENTIFIER} android ./build_android/CodePush --deployment ${ENVIRONMENT} --dist ${ANDROID_VERSION}
# Only works on macOS
# sentry-cli --auth-token=${SENTRY_AUTH_TOKEN} react-native appcenter --org corviva --project workly ${APPCENTER_IOS_IDENTIFIER} ios ./build_ios/CodePush --deployment ${ENVIRONMENT} --bundle-id ${IOS_BUNDLE_ID} --dist ${IOS_VERSION}